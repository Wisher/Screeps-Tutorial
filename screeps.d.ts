declare global {
    interface CreepMemory {
        [name: string]: any;
        role: string;
        building?: boolean;
        upgrading?: boolean;
        target?: Id<_HasId>;
        harvesting?: boolean;
        source?: Id<Source>;
    }

    interface Memory {
        creeps: { [name: string]: CreepMemory };
    }
}

export {};
