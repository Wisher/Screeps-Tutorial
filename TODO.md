# TODO List

 - [ ] Clean-up Main
   - [x] Move spawning logic into separate file
     - [x] Simple dynamic auto-spawning of creeps by role
   - [x] Move tower logic into separate file
     - [x] Make tower logic tower agnostic
 - [ ] Repairer Role
 - [ ] Refactor/Add functionality to Harvester
   - [x] Make Harvester target all fillable structures
   - [x] Give harvester target prioritization
   - [x] make harvester target a specific random ~~valid~~ source
   - [ ] add logic to source targeting, e.g. not empty
 - [x] Linting
   - [x] ESLint
   - [x] Prettier