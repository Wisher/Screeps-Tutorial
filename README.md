# Screeps

To get started either follow along with this [video series](https://www.youtube.com/playlist?list=PL1HXCI0NdzvIdyfBVae_TNfaODe5GRSm2)

or download the files and run the following commands, you will need to have git and nodejs installed on your system
```bash
git init
npm install @types/screeps
npm install @types/lodash
npm install @types/node
npm install -g typescript
```

[Screeps api documentation](https://docs.screeps.com/api/)

If you want ESLint/Prettier Support also run the following commands

```bash
npm install eslint@^8
npm install eslint-config-prettier
npm install eslint-import-resolver-typescript
npm install eslint-plugin-import
npm install eslint-plugin-prettier
npm install @typescript-eslint/parser 
npm install @typescript-eslint/eslint-plugin
npm install @typescript-eslint/typescript-estree
npm install prettier
```

Afterwards you will need to configure your editor/IDE to utilise ESLint and Prettier